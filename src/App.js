import React, { Component } from 'react'
import QrReader from 'react-qr-scanner'
import './App.css';

class App extends React.Component {

    constructor(props){
      super(props)
      this.state = {
        delay: 100,
        result: 'No result',
      }

    }

    handleScan = (data) => {
      this.setState({
        result: data,
      })
    }

    handleError = (err) => {
      console.error(err)
    }
  
    render(){

      return (
        <div className="App">
          <header className="App-header">
              <div>
                <QrReader
                  delay={this.state.delay}
                  style={{height: 240,width: 320}}
                  onError={this.handleError}
                  onScan={this.handleScan}
                  />
                <p>{this.state.result}</p>
              </div>
          </header>
        </div>
      );
    }

}

export default App;
